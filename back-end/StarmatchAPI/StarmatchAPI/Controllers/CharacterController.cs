﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StarmatchAPI.Business;
using StarmatchAPI.Models;
using StarmatchAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarmatchAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CharactersController : ControllerBase
    {

        private readonly CharacterBusiness _characterBusiness;

        public CharactersController(CharacterBusiness characterBusiness)
        {
            _characterBusiness = characterBusiness;
        }

        [HttpGet]
        public IEnumerable<Character> GetAllCharacters()
        {
            return _characterBusiness.GetAllCharacters();
        }

        [HttpGet("{characterCount}/random")]
        public IEnumerable<Character> GetRandomCharacters(int characterCount)
        {
            return _characterBusiness.GetRandomCharacters(characterCount);
        }

        [HttpPut("{idCharacter}/like")]
        public void LikeCharacter(string idCharacter)
        {
            _characterBusiness.LikeCharacter(idCharacter);
        }

    }
}
