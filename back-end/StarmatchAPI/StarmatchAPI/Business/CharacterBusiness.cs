﻿using StarmatchAPI.Models;
using StarmatchAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarmatchAPI.Business
{
    public class CharacterBusiness
    {
        private readonly CharacterService _characterService;

        public CharacterBusiness(CharacterService characterService)
        {
            _characterService = characterService;
        }

        public IEnumerable<Character> GetAllCharacters()
        {
            return _characterService.GetAllCharacters();
        }


        public IEnumerable<Character> GetRandomCharacters(int characterCount)
        {
            return _characterService.GetRandomCharacters(characterCount);
        }
        public void LikeCharacter(string idCharacter)
        {
            Character character = _characterService.Get(idCharacter);

            if (character == null)
            {
                return;
            }
            character.LikeCount++;

            _characterService.Update(idCharacter, character);
        }



    }
}
