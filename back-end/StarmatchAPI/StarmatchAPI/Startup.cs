using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using StarmatchAPI.Business;
using StarmatchAPI.Services;
using StarmatchAPI.Settings;

namespace StarmatchAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
            ConventionRegistry.Register("camelCase", conventionPack, t => true);


            #region DB Settings
            services.Configure<StarmatchDatabaseSettings>(
                Configuration.GetSection(nameof(StarmatchDatabaseSettings)));

            services.AddSingleton<IStarmatchDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<StarmatchDatabaseSettings>>().Value);
            #endregion
            
            #region Services
            services.AddSingleton<CharacterService>();
            #endregion

            #region Business
            services.AddScoped<CharacterBusiness>();
            #endregion

            services.AddCors(options =>
            {
                options.AddPolicy("AllowBlobAndLocal",
                    builder =>
                    {
                        builder
                        .WithOrigins("http://localhost:4200", "https://starmatchstorageaccount.z28.web.core.windows.net")
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("AllowBlobAndLocal");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
