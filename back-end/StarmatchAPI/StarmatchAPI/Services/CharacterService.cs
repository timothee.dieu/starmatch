﻿using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using StarmatchAPI.Models;
using StarmatchAPI.Settings;
using System.Collections.Generic;
using System.Linq;

namespace StarmatchAPI.Services
{
    public class CharacterService
    {
        private readonly IMongoCollection<Character> _characters;

        public CharacterService(IStarmatchDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _characters = database.GetCollection<Character>(settings.CharactersCollectionName);
        }

        public List<Character> GetAllCharacters()
        { 
            return _characters.Find(character => true)
                .ToList();
        }

        public List<Character> GetRandomCharacters(int characterCount)
        { 
            return _characters.AsQueryable()
                .Sample(characterCount)
                .ToList();
        }


        public Character Get(string id)
        { 
            return _characters.Find<Character>(character => character.Id == id)
                .FirstOrDefault();
        }
            

        public Character Create(Character character)
        {
            _characters.InsertOne(character);
            return character;
        }

        public void Update(string id, Character characterIn)
        {
            _characters.ReplaceOne(character => character.Id == id, characterIn);
        }

        public void Remove(Character characterIn)
        { 
            _characters.DeleteOne(character => character.Id == characterIn.Id);
        }

        public void Remove(string id)
        {
            _characters.DeleteOne(character => character.Id == id);
        }
    }
}
