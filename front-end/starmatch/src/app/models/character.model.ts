export class Character {
	id: string;
	name: string;
	pic: string;
	homeworld: string;
	likeCount: number;
}