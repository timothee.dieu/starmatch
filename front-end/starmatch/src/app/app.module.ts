import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import { AppComponent } from './app.component';
import { CharacterCardComponent, CharacterChooserComponent, CharacterRankingComponent, HomeComponent, TopBarComponent } from './components';

@NgModule({
  declarations: [
    AppComponent,
	HomeComponent,
	TopBarComponent,
	CharacterChooserComponent,
	CharacterRankingComponent,
	CharacterCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	HttpClientModule,
	BrowserAnimationsModule,
	MatButtonToggleModule,
	MatCardModule,
	MatButtonModule,
	MatIconModule,
	MatToolbarModule,
	MatTabsModule,
	MatTableModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
