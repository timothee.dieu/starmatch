import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Character } from 'src/app/models';

@Component({
	selector: 'app-character-card',
	templateUrl: './character-card.component.html',
	styleUrls: ['./character-card.component.scss']
})
export class CharacterCardComponent implements OnInit {
	@Input() character: Character;
	@Output() like: EventEmitter<Character> = new EventEmitter();

	constructor() { }

	ngOnInit() {
	}

	onLike(character: Character) {
		this.like.emit(character);
	}

}
