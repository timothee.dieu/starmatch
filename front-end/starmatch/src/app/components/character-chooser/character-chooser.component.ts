import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/models';
import { CharacterService } from 'src/app/services';

@Component({
	selector: 'app-character-chooser',
	templateUrl: './character-chooser.component.html',
	styleUrls: ['./character-chooser.component.scss']
})
export class CharacterChooserComponent implements OnInit {

	characters: Character[];
	isLoading: boolean;

	constructor(private characterService: CharacterService) { }

	ngOnInit() {
		this.loadCharacters();
	}

	loadCharacters()  {
		this.isLoading = true;
		this.characterService.getRandomCharacters$(2).subscribe({
			next: (characters) => {
				this.characters = characters;
				this.isLoading = false;
			},
			error: () => {

			}
		})
	}

	chooseCharacter(idCharacter: string) {
		this.characterService.likeCharacter$(idCharacter).subscribe({
			next: () => {
				this.loadCharacters();
			},
			error: () => {

			}
		})
	}

}
