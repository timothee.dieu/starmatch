import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/models';
import { CharacterService } from 'src/app/services';

@Component({
	selector: 'app-character-ranking',
	templateUrl: './character-ranking.component.html',
	styleUrls: ['./character-ranking.component.scss']
})
export class CharacterRankingComponent implements OnInit {

	displayedColumns: string[] = ['pic', 'name', 'likeCount'];
  	dataSource: Character[] = [];

	constructor(private characterService: CharacterService) { }

	ngOnInit() {
		this.loadCharacters();
	}

	loadCharacters()  {
		this.characterService.getAllCharacters$().subscribe({
			next: (characters) => {
				this.dataSource = characters.sort((c1, c2) => c2.likeCount - c1.likeCount)
				console.log(characters)
			},
			error: () => {

			}
		})
	}
}
