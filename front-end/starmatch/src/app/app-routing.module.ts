import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CharacterRankingComponent } from './components/character-ranking/character-ranking.component';

const routes: Routes = [
	{
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
	{
        path: 'home',
		component: HomeComponent
    },
	{
        path: 'ranking',
		component: CharacterRankingComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
