import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Character } from '../models';

@Injectable({
	providedIn: 'root'
})
export class CharacterService {

	API_URL: string = `${environment.apiUrl}/characters`;

	constructor(private http: HttpClient) { }

	getAllCharacters$(): Observable<Character[]> {
		return this.http.get<Character[]>(`${this.API_URL}`);
	}

	getRandomCharacters$(characterCount: number): Observable<Character[]> {
		return this.http.get<Character[]>(`${this.API_URL}/${characterCount}/random`);
	}

	likeCharacter$(idCharacter: string): Observable<Character[]> {
		return this.http.put<Character[]>(`${this.API_URL}/${idCharacter}/like`, {});
	}

}
