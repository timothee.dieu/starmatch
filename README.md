# StarMarch

A simple [Web Application](https://starmatchstorageaccount.z28.web.core.windows.net/) to vote for your favorite StarWars character.


## Requirements
- [Visual Studio](https://visualstudio.microsoft.com/fr/vs/community/)
- [Angular CLI](https://cli.angular.io/)


## Installation & Launch
From the root of the repository

Front-End:


```bash
cd ./front-end/starmatch
npm install
npm run start
```


Back-End:
- Open `starmatch\back-end\StarmatchAPI\StarmatchAPI.sln`
- Start the project





## License
[MIT](https://choosealicense.com/licenses/mit/)
